<footer class="footer">
    <div class="container">
      <div class="row footer__wrapper">
        <div class="logo col-lg-3 col-md-3 col-6">
          <div class="logo__link">
            <?php the_custom_logo(); ?>
          </div>
        </div>
        <ul class="footer__catalog-footer catalog-footer list-reset col-lg-3 col-md-3 col-6 mb-4">
          <li class="catalog-footer__title">Каталог продукции</li>
          <li class="catalog-footer__item"><a href="#" class="catalog-footer__link">Пельмени</a></li>
          <li class="catalog-footer__item"><a href="#" class="catalog-footer__link">Манты</a></li>
          <li class="catalog-footer__item"><a href="#" class="catalog-footer__link">Вареники</a></li>
          <li class="catalog-footer__item"><a href="#" class="catalog-footer__link">Чебуреки</a></li>
          <li class="catalog-footer__item"><a href="#" class="catalog-footer__link">Голубцы</a></li>
        </ul>
        <ul class="footer__legal-footer legal-footer list-reset col-lg-3 col-md-3 col-12 mb-4">
          <li class="legal-footer__title">Юридическая информация</li>
          <li class="legal-footer__item"><a href="/sertifikaty" class="legal-footer__link">Сертификаты</a></li>
          <li class="legal-footer__item"><a href="<?php the_field('btn_horeca_down_1'); ?>" download class="legal-footer__link">Договор оферты</a></li>
          <li class="legal-footer__item"><a href="/karera-2" class="legal-footer__link">Карьера</a></li>
        </ul>
        <ul class="footer__contact-footer contact-footer list-reset col-lg-3 col-md-3 cols-12 mb-4">
          <li class="contact-footer__title">Контакты</li>
          <li class="contact-footer__item">
            <!-- Добавляем ввод адреса черех админку -->
            <address class="contact-footer__addres"><?php the_field('address_info', 2); ?></address>
          </li>
          <li class="contact-footer__item"><a href="tel:+73523328465" class="contact-footer__link"><?php the_field('tel_1', 2); ?></a>
          </li>
          <li class="contact-footer__item"><a href="tel:+79225600044" class="contact-footer__link"><?php the_field('tel_2', 2); ?></a>
          </li>
          <li class="contact-footer__item"><a href="tel:+79226762222" class="contact-footer__link"><?php the_field('tel_3', 2); ?></a>
          </li>
          <li class="contact-footer__item"><a href="email:info@icegrad.com"
              class="contact-footer__link"><?php the_field('mail', 2); ?></a></li>
        </ul>
      </div>
    </div>
  </footer>

  <?php
    wp_footer();
  ?>

</body>

</html>
