<?php
  /*
  Template Name: HoReCa
  */
?>

<?php
  get_header();
?>

<body>
  <main>
    <section class="horeca">
      <div class="container">
        <div class="row">
          <h1 class="horeca__title col-lg-6 col-md-6"><?php the_field('horeca_title'); ?></h1>
        </div>
        <div class="row horeca__wrapper">
          <p class="horeca__text col-lg-8 col-md-8">
           <?php the_field('descr_horeca_0'); ?>
          </p>
          <div class="col-lg-4 col-md-4">
            <!-- НУЖНО ДОБАВИТЬ ПОЛЕ В АДМИИНКЕ ДЛЯ УПРАВЛЕНИЯ ССЫЛКОЙ НА СКАЧИВАНИЕ -->
            <a href="<?php the_field('btn_horeca_down'); ?>" download class="horeca__btn"><?php the_field('btn_horeca'); ?></a>
          </div>

        </div>
        <div class="row">
          <img src="<?php the_field('img_horeca'); ?>" alt="" class="horeca__bg">
        </div>
      </div>
    </section>

    <section class="partner">
      <div class="container">
        <div class="row">
          <h2 class="partner__title col-lg-6 col-md-6"><?php the_field('subtitle_horeca_1'); ?></h2>
        </div>
        <div class="row partner__steps">
          <div class="stap-partner col-lg-5 col-md-5">
            <div class="stap-partner__header">
              <h5 class="stap-partner__title"><span>Шаг 1.</span>Регистрация и подача
                заявки</h5>
            </div>
            <div class="stap-partner__bottom">
              <p class="stap-partner__text">Расскажите нам о себе и вашем бизнесе — заполните заявку, чтобы стать нашим
                партнером. Это займет не
                более двух минут!</p>
              <button class="stap-partner__btn stap-partner__btn--popup">Подать заявку</button>
            </div>
            <div class="wrap-form">
                          <form action="POST" class="form-product">
              <?php echo do_shortcode('[contact-form-7 id="406" title="Заявка на заказ продукции"]'); ?>
            </form>
            </div>

          </div>

          <div class="stap-partner col-lg-5 col-md-5">
            <div class="stap-partner__header">
              <h5 class="stap-partner__title"><span>Шаг 2.</span>Оформление документов
              </h5>
            </div>
            <div class="stap-partner__bottom">
              <p class="stap-partner__text">Подготовьте необходимые документы для заключения договора и станьте нашим
                партнером.</p>
              <button download class="stap-partner__btn"><a class="btn_hover" href="<?php the_field('btn_horeca_down'); ?>">Смотреть список документов</a></button>
            </div>
          </div>
          <div class="stap-partner stap-partner--center col-lg-2 col-md-2">
            <div class="stap-partner__header">
              <h5 class="stap-partner__title"><span>Шаг 3.</span>Наслаждаемся
                плодотворным сотрудничеством</h5>
            </div>
          </div>
        </div>
        <div class="row">
          <p class="stap-partner__text col-12"><?php the_field('descr_horeca_1'); ?></p>
        </div>

        <ul class="partner-advantage row list-reset">
          <li class="partner-advantage__item col-lg-3 col-md-6">
            <h5 class="partner-advantage__title"><span>01</span><?php the_field('one_advantage_subtitle'); ?></h5>
            <p class="partner-advantage__text"><?php the_field('one_advantage_descr'); ?></p>
          </li>
          <li class="partner-advantage__item col-lg-3 col-md-6">
            <h5 class="partner-advantage__title"><span>02</span><?php the_field('two_advantage_subtitle'); ?></h5>
            <p class="partner-advantage__text"><?php the_field('two_advantage_descr'); ?></p>
          </li>
          <li class="partner-advantage__item col-lg-3 col-md-6">
            <h5 class="partner-advantage__title"><span>03</span><?php the_field('three_advantage_subtitle'); ?></h5>
            <p class="partner-advantage__text"><?php the_field('three_advantage_descr'); ?></p>
          </li>
          <li class="partner-advantage__item col-lg-3 col-md-6">
            <h5 class="partner-advantage__title"><span>04</span><?php the_field('four_advantage_subtitle'); ?></h5>
            <p class="partner-advantage__text"><?php the_field('four_advantage_descr'); ?></p>
          </li>
        </ul>
      </div>
    </section>

    <section class="questions">
      <div class="container">
        <div class="row">
          <h2 class="questions__title"><?php the_field('subtitle_horeca_2'); ?></h2>
        </div>
        <div class="row">
          <div class="accordion-questions">
            <div class="row accordion-questions__wrapper">
              <div class="accordion-questions__title col-lg-11 col-md-11 col-11"><?php the_field('one_tab_accordion_subtitle'); ?></div>
              <span class="accordion-questions__btn"></span>
            </div>
            <p class="accordion-questions__text"><?php the_field('one_tab_accordion_descr'); ?></p>
          </div>
          <div class="accordion-questions">
            <div class="row accordion-questions__wrapper">
              <div class="accordion-questions__title col-lg-11 col-md-11 col-11"><?php the_field('two_tab_accordion_subtitle'); ?>
              </div>
              <span class="accordion-questions__btn"></span>
            </div>
            <p class="accordion-questions__text"><?php the_field('two_tab_accordion_descr'); ?></p>
          </div>
          <div class="accordion-questions">
            <div class="row accordion-questions__wrapper">
              <div class="accordion-questions__title col-lg-11 col-md-11 col-11"><?php the_field('three_tab_accordion_subtitle'); ?>
              </div>
              <span class="accordion-questions__btn"></span>
            </div>
            <p class="accordion-questions__text"><?php the_field('three_tab_accordion_descr'); ?></p>
          </div>
          <div class="accordion-questions">
            <div class="row accordion-questions__wrapper">
              <div class="accordion-questions__title col-lg-11 col-md-11 col-11"><?php the_field('four_tab_accordion_subtitle'); ?>
              </div>
              <span class="accordion-questions__btn"></span>
            </div>
            <p class="accordion-questions__text"><?php the_field('four_tab_accordion_descr'); ?></p>
          </div>
        </div>
      </div>
    </section>

  </main>

</body>

<?php
  get_footer();
?>

