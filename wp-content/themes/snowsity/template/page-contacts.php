<?php
  /*
  Template Name: Контакты
  */
?>

<?php
  get_header();
?>

<body>
  <section class="page-contacts">
    <div class="container">
      <div class="row">
        <h1 class="page-contacts__title"><?php the_field('title_contacts'); ?></h1>
      </div>
      <div class="row mb-lg-4 mb-md-4 mb-2">
        <div class="offece-info col-lg-6 col-md-6">
          <span class="offece-info__title"><?php the_field('subtitle_production'); ?></span>
          <ul class="offece-info__list list-reset">
            <li class="offece-info__item"><span>Адрес:</span>
              <address><?php the_field('address_production'); ?></address>
            </li>
            <li class="offece-info__item"><span>Телефон:</span><a href="tel:+73523328465"><?php the_field('city_tel'); ?></a> </li>
            <li class="offece-info__item"><span>Моб. Телефон:</span><a href="tel:+79225600011"><?php the_field('mobil_tel'); ?></a>
            </li>
            <li class="offece-info__item"><span>E-mail: </span><a href="mailto:icegrad@mail.ru"><?php the_field('email_production'); ?></a>
            </li>
          </ul>
        </div>
        <div class="office-map col-lg-6 col-md-6">
        <div id="map-contact-product" style="width: 100%; height: 100% "></div>
        </div>
      </div>
      <div class="row mb-lg-4 mb-md-4 mb-2">
        <div class="offece-info col-lg-6 col-md-6">
          <span class="offece-info__title"><?php the_field('subtitle_sale'); ?></span>
          <ul class="offece-info__list list-reset">
            <li class="offece-info__item"><span>Адрес:</span>
              <address><?php the_field('address_sale'); ?></address>
            </li>
            <li class="offece-info__item"><span>Телефон:</span><a href="tel:+73523328465"><?php the_field('city_tel_1'); ?></a> </li>
            <li class="offece-info__item"><span>Моб. Телефон:</span><a href="tel:+79225600044"><?php the_field('mobil_tel_1'); ?></a>
            </li>
            <li class="offece-info__item"><span>Моб. Телефон:</span><a href="tel:+79226762222"><?php the_field('mobil_tel_2'); ?></a>
            </li>
            <li class="offece-info__item"><span>Режим рыботы:</span><?php the_field('time_sale'); ?></li>

            <li class="offece-info__item"><span>E-mail: </span><a href="mailto:icegrad@mail.ru"><?php the_field('email_sale'); ?></a>
            </li>
          </ul>
        </div>
        <div class="office-map col-lg-6 col-md-6" style =" overflow: hidden;">
        <div id="map-contact-office" style="width: 100%; height: 100% "></div>
        </div>
      </div>
    </div>
  </section>
</body>

</html>

<?php
  get_footer();
?>

