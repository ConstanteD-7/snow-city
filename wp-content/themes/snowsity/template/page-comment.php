<?php
  /*
  Template Name: Отзывы
  */
?>

<?php
  get_header();
?>

<body>
  <section class="page-comment">
    <div class="container">
      <div class="row">
        <h1 class="page-comment__title col-lg-7 col-md-7"><?php the_field('reviews'); ?></h1>
        <p class="page-comment__text"><?php the_field('reviews_descr'); ?></p>
      </div>
      <div class="row">

        <?php
          // параметры по умолчанию
          $my_posts = get_posts( array(
            'numberposts' => -1,
            'category_name'    => 'reviews_1',
            'orderby'     => 'date',
            'order'       => 'ASC',
            'post_type'   => 'post',
            'suppress_filters' => true, // подавление работы фильтров изменения SQL запроса
          ) );



          foreach( $my_posts as $post ){
            setup_postdata( $post );
            ?>

            <div class="card-comment card-comment--revers">
              <div class="card-comment__client-wrapper col-lg-2 col-md-2">
                <img src="<?php the_field('foto'); ?>" alt="фото клиента" class="card-comment__img">
                <span class="card-comment__name"><?php the_field('name_people'); ?></span>
                <span class="card-comment__descr"><?php the_field('company_name'); ?></span>
              </div>
              <div class="card-comment__comment col-lg-10 col-md-10 "><span></span><?php the_field('reviews_people'); ?> </div>
            </div>

            <?php
          }

          wp_reset_postdata(); // сброс
        ?>
      </div>
    </div>
  </section>
</body>

</html>

<?php
  get_footer();
?>
