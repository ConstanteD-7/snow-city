<?php
  /*
  Template Name: О компании
  */
?>

<?php
  get_header();
  ?>

  <section class="about-company">
    <div class="container">
      <div class="row">
        <h1 class="about-company__title"><?php the_field('company_title'); ?></h1>
        <p class="about-company__text"><?php the_field('company_descr'); ?></p>
      </div>
      <div class="row">
        <img src="<?php the_field('company_img'); ?>" alt="Пельмени ручно работы"
          class="about-company__big-img">
      </div>
      <div class="row advantage">
        <ul class="advantage__list list-reset">
          <li class="advantage__item advantage__item--icon-equipment col-lg-3 col-md-3 ">Отборное отечественное сырье</li>
          <li class="advantage__item advantage__item--icon-homemade-recipes col-lg-3 col-md-3 ">Традиционные домашние рецепты</li>
          <li class="advantage__item advantage__item--icon-label-products col-lg-3 col-md-3 ">Высокие вкусовые свойства</li>
          <li class="advantage__item advantage__item--icon-like col-lg-3 col-md-3 ">Продукт полезен и&nbsp;безопасен для здоровья
          </li>
          <li class="advantage__item advantage__item--icon-prices col-lg-3 col-md-3 ">Цены без посреднических надбавок</li>
          <li class="advantage__item advantage__item--icon-production col-lg-3 col-md-3 ">Производственные цеха</li>
          <li class="advantage__item advantage__item--icon-security-shield col-lg-3 col-md-3 ">Промышленное обрудование</li>
          <li class="advantage__item advantage__item--icon-selected-meat col-lg-3 col-md-3 ">Собственная упаковочная и&nbsp;этикетная
            продукция</li>
        </ul>
      </div>
      <div class="row">
        <p class="about-company__text">
          <img src="<?php the_field('company_img_right'); ?>" alt="Пельмени ручно работы" class="about-company__img col-5 col-md-12"><?php the_field('company_descr_left'); ?>
      </div>
      <div class="row">
        <p class="about-company__text"><?php the_field('company_descr_2'); ?></p>
        <div class="row">
          <div class="gallery">
            <ul class="gallery__list list-reset">

              <?php
                // параметры по умолчанию
                $my_posts = get_posts( array(
                  'numberposts' => -1,
                  'category_name'    => 'diploma_certificates',
                  'orderby'     => 'date',
                  'order'       => 'ASC',
                  'post_type'   => 'post',
                  'suppress_filters' => true, // подавление работы фильтров изменения SQL запроса
                ) );



                foreach( $my_posts as $post ){
                  setup_postdata( $post );
                  ?>

                  <li class="gallery__item col-3"><img src="<?php the_field('diploma_img'); ?>" alt="диплом"
                  class="gallery__img"></li>

                  <?php
                }

                wp_reset_postdata(); // сброс
              ?>

            </ul>
          </div>
        </div>
        <div class="row">
          <p class="about-company__text"><?php the_field('company_descr_diploma'); ?></p>
        </div>
      </div>
    </div>
  </section>


<?php
  get_footer();
?>

