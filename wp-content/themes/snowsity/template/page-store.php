<?php
  /*
  Template Name: Наши магазины
  */
?>

<?php
  get_header();
?>

<body>
  <section class="page-store">
    <div class="container">
      <div class="row">
        <h1 class="page-store__title col-lg-7 col-md-7">Нашу продукцию можно найти в&nbsp;городах России</h1>
      </div>
    </div>
    <div class="container-fluid">
      <div class="row">
        <div class="col">
        <div class="buy-where maps" id="map" style="width: 100%; height: 432px">
            <div class="maps__maps-info maps-info">
              <span class="maps-info__title">Магазины сети:</span>
              <input class="maps-info__search" type="search" name="maps-info__search" id="maps-info__search"
                placeholder="Начните вводить адрес">
              <button class="maps-info__btn"></button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</body>

</html>

<?php
  get_footer();
?>
