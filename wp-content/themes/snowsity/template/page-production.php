<?php
  /*
  Template Name: Производство
  */
?>

<?php
  get_header();
?>

<body>
  <section class="page-production">
    <div class="container">
      <div class="row">
        <h1 class="page-production__title col-lg-9 col-md-9"><?php the_field('production_title'); ?></h1>
      </div>
      <div class="row">
        <p class="page-production__text"><?php the_field('production_descr'); ?></p>
      </div>
      <div class="row">
        <div class="power page-production__power col-lg-3 col-md-3">
          <span class="power__title"><?php the_field('indicator_1'); ?></span>
          <p class="power__text"><?php the_field('indicator_descr_1'); ?></p>
        </div>
        <div class="power page-production__power col-lg-3 col-md-3">
          <span class="power__title"><?php the_field('indicator_2'); ?></span>
          <p class="power__text"><?php the_field('indicator_descr_2'); ?></p>
        </div>
        <div class="power page-production__power col-lg-3 col-md-3">
          <span class="power__title"><?php the_field('indicator_3'); ?></span>
          <p class="power__text"><?php the_field('indicator_descr_3'); ?></p>
        </div>
        <div class="power page-production__power col-lg-3 col-md-3">
          <span class="power__title"><?php the_field('indicator_4'); ?></span>
          <p class="power__text"><?php the_field('indicator_descr_4'); ?></p>
        </div>
      </div>

      <div class="row">

        <?php
          // параметры по умолчанию
          $my_posts = get_posts( array(
            'numberposts' => -1,
            'category_name'    => 'production',
            'orderby'     => 'date',
            'order'       => 'ASC',
            'post_type'   => 'post',
            'suppress_filters' => true, // подавление работы фильтров изменения SQL запроса
          ) );

          foreach( $my_posts as $post ){
            setup_postdata( $post );
            ?>

              <div class="card-equipment">

                <div class="row">
                    <span class="card-equipment__title col-lg-7 col-md-7"><?php the_field('title_production_heading'); ?></span>

                    <div class="row card-equipment__revers">
                      <img src="<?php the_field('img_production_heading'); ?>" alt="" class="card-equipment__img col-lg-6 col-md-6">
                    <div class="card-equipment__description description-equipment col-lg-6 col-md-6">
                      <span class="description-equipment__title"><?php the_field('subtitle_production_heading'); ?></span>
                      <p class="description-equipment__text"><?php the_field('descr_production_heading'); ?></p>
                      <ul class="description-equipment__list">
                        <li class="description-equipment__item"><?php the_field('1_indicator_production_heading'); ?></li>
                        <li class="description-equipment__item"><?php the_field('2_indicator_production_heading'); ?></li>
                        <li class="description-equipment__item"><?php the_field('3_indicator_production_heading'); ?></li>
                        <li class="description-equipment__item"><?php the_field('4_indicator_production_heading'); ?></li>
                        <li class="description-equipment__item"><?php the_field('5_indicator_production_heading'); ?></li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>

            <?php
          }

          wp_reset_postdata(); // сброс
        ?>
      </div>


      <div class="row card-equipment__revers"> <div class="page-product__wrapper-form">
        <div class="col-lg-5 col-md-5 page-product__form">
          <h5 class="page-product__form-title">Оставьте заявку на&nbsp;заказ продукции</h5>
          <form action="POST" class="form-product">
            <?php echo do_shortcode('[contact-form-7 id="406" title="Заявка на заказ продукции"]'); ?>
            <!-- <input id="form-product__name" class="form-product__name" type="text" placeholder="Имя">
            <input id="form-product__tel" class="form-product__tel" type="tel" placeholder="Телефон">
            <textarea id="form-product__textarea" name="form-product__textarea" cols="30" rows="1"
              placeholder="Комменатрий"></textarea>
            <input class="form-product__btn btn" type="submit" value="Оформить заказ"> -->
          </form>
        </div>

        <div class="product-video col-lg-6 col-md-6">
          <video width="100%"  controls="controls" loop preload="none" poster="<?php bloginfo('template_url'); ?>/assets/img/poster-video.jpg">
            <source src="<?php the_field('video_production', 2); ?>">
          </video>
        </div>

      </div>
    </div>
  </section>
</body>

</html>


<?php
  get_footer();
?>
