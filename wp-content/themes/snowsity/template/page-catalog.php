<?php
  /*
  Template Name: Каталог
  */
?>


<?php
  get_header();
?>


<body>

  <section class="catalog">
    <div class="container">
      <div class="row">
        <h1 class="catalog__title"><?php the_field('product_title'); ?></h1>
      </div>
      <div class="row">
        <div class="catalog__teg teg">
          <ul class="tag__list">
            <li class="list-reset tag__item"><a href="#" class="tag__link">
              <?php
                if ( function_exists('yoast_breadcrumb') ) {
                  yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
                }
              ?>
            </li>
            <!-- <li class="list-reset tag__item"><a href="#" class="tag__link">пельмени&nbsp;/</a></li>
            <li class="list-reset tag__item"><a href="#" class="tag__link">фарш&nbsp;/</a></li>
            <li class="list-reset tag__item"><a href="#" class="tag__link">хинкали&nbsp;/</a></li>
            <li class="list-reset tag__item"><a href="#" class="tag__link">фрикадельки&nbsp;/</a></li>
            <li class="list-reset tag__item"><a href="#" class="tag__link">тефтели&nbsp;/</a></li>
            <li class="list-reset tag__item"><a href="#" class="tag__link">зразы&nbsp;/</a></li>
            <li class="list-reset tag__item"><a href="#" class="tag__link">люля-кебаб&nbsp;/</a></li>
            <li class="list-reset tag__item"><a href="#" class="tag__link">купаты&nbsp;/</a></li>
            <li class="list-reset tag__item"><a href="#" class="tag__link">чебуреки&nbsp;/</a></li>
            <li class="list-reset tag__item"><a href="#" class="tag__link">шницель&nbsp;/</a></li>
            <li class="list-reset tag__item"><a href="#" class="tag__link">голубцы&nbsp;/</a></li>
            <li class="list-reset tag__item"><a href="#" class="tag__link">свинина&nbsp;/</a></li>
            <li class="list-reset tag__item"><a href="#" class="tag__link">говдяина&nbsp;/</a></li>
            <li class="list-reset tag__item"><a href="#" class="tag__link">баранина&nbsp;/</a></li>
            <li class="list-reset tag__item"><a href="#" class="tag__link">курица</a></li> -->
          </ul>
        </div>
      </div>
    </div>
  </section>

  <section class="vareniki">
    <div class="container">
      <div class="row">
        <h2 class="vareniki__title"><?php the_field('name_product'); ?></h2>
        <div class="vareniki__wrapper">

        <?php
                // параметры по умолчанию
                $my_posts = get_posts( array(
                  'numberposts' => -1,
                  'category_name'    => 'product_0',
                  'orderby'     => 'date',
                  'order'       => 'ASC',
                  'post_type'   => 'post',
                  'suppress_filters' => true, // подавление работы фильтров изменения SQL запроса
                ) );



                foreach( $my_posts as $post ){
                  setup_postdata( $post );
                  ?>

                  <div class="card-product col">
                    <img src="<?php the_field('image_product'); ?>" alt="Изображеие товара" class="card-product__img">
                    <div class="card-product__content">
                      <span class="card-product__title"><?php the_title( ); ?></span>
                      <span class="card-product__description"><?php the_field('weight_product'); ?></span>
                      <a href="<?php echo get_permalink(); ?>" class="card-product__btn-link btn-link"><?php the_field('link_product'); ?></a>
                    </div>
                  </div>

                  <?php
                }

                wp_reset_postdata(); // сброс
              ?>

        </div>
      </div>
    </div>
  </section>
  <section class="pelmeni">
    <div class="container">
      <div class="row">
        <h2 class="pelmeni__title"><?php the_field('name_product_2'); ?></h2>
      </div>
      <div class="row">
        <div class="pelmeni__wrapper">
        <?php
                // параметры по умолчанию
                $my_posts = get_posts( array(
                  'numberposts' => -1,
                  'category_name'    => 'product_1',
                  'orderby'     => 'date',
                  'order'       => 'ASC',
                  'post_type'   => 'post',
                  'suppress_filters' => true, // подавление работы фильтров изменения SQL запроса
                ) );



                foreach( $my_posts as $post ){
                  setup_postdata( $post );
                  ?>

                  <div class="card-product col">
                    <img src="<?php the_field('image_product'); ?>" alt="Изображеие товара" class="card-product__img">
                    <div class="card-product__content">
                      <span class="card-product__title"><?php the_title( ); ?></span>
                      <span class="card-product__description"><?php the_field('weight_product'); ?></span>
                      <a href="<?php echo get_permalink(); ?>" class="card-product__btn-link btn-link"><?php the_field('link_product'); ?></a>
                    </div>
                  </div>

                  <?php
                }

                wp_reset_postdata(); // сброс
              ?>

        </div>
      </div>
    </div>
  </section>
  <section class="ground-meat">
    <div class="container">
      <div class="row">
        <h2 class="ground-meat__title"><?php the_field('name_product_3'); ?></h2>
      </div>
      <div class="ground-meat__wrapper">
        <?php
                // параметры по умолчанию
                $my_posts = get_posts( array(
                  'numberposts' => -1,
                  'category_name'    => 'product_2',
                  'orderby'     => 'date',
                  'order'       => 'ASC',
                  'post_type'   => 'post',
                  'suppress_filters' => true, // подавление работы фильтров изменения SQL запроса
                ) );



                foreach( $my_posts as $post ){
                  setup_postdata( $post );
                  ?>

                  <div class="card-product col">
                    <img src="<?php the_field('image_product'); ?>" alt="Изображеие товара" class="card-product__img">
                    <div class="card-product__content">
                      <span class="card-product__title"><?php the_title( ); ?></span>
                      <span class="card-product__description"><?php the_field('weight_product'); ?></span>
                      <a href="<?php echo get_permalink(); ?>" class="card-product__btn-link btn-link"><?php the_field('link_product'); ?></a>
                    </div>
                  </div>

                  <?php
                }

                wp_reset_postdata(); // сброс
              ?>

      </div>
    </div>
  </section>
  <section class="khinkali">
    <div class="container">
      <div class="row">
        <h2 class="khinkali__title"><?php the_field('name_product_4'); ?></h2>
        <div class="khinkali__wrapper">
          <?php
                // параметры по умолчанию
                $my_posts = get_posts( array(
                  'numberposts' => -1,
                  'category_name'    => 'product_3',
                  'orderby'     => 'date',
                  'order'       => 'ASC',
                  'post_type'   => 'post',
                  'suppress_filters' => true, // подавление работы фильтров изменения SQL запроса
                ) );



                foreach( $my_posts as $post ){
                  setup_postdata( $post );
                  ?>

                  <div class="card-product col">
                    <img src="<?php the_field('image_product'); ?>" alt="Изображеие товара" class="card-product__img">
                    <div class="card-product__content">
                      <span class="card-product__title"><?php the_title( ); ?></span>
                      <span class="card-product__description"><?php the_field('weight_product'); ?></span>
                      <a href="<?php echo get_permalink(); ?>" class="card-product__btn-link btn-link"><?php the_field('link_product'); ?></a>
                    </div>
                  </div>

                  <?php
                }

                wp_reset_postdata(); // сброс
              ?>
        </div>
      </div>
    </div>
  </section>

</body>

</html>

<?php
  get_footer();
?>

