<?php
  /*
  Template Name: Карьера
  */
?>


<?php
  get_header();
?>

<body>
  <section class="page-career">
    <div class="container">
      <div class="row">
        <div class="page-career__career-block career-block ">
          <div class="row">
            <h2 class="career-block__title"><?php the_field('title_career'); ?></h2>
          </div>

          <ul class="career-block__list row">
            <li class="career-block__item col-lg-6 col-md-6"><?php the_field('1_career'); ?></li>
            <li class="career-block__item col-lg-6 col-md-6"><?php the_field('2_career'); ?></li>
            <li class="career-block__item col-lg-6 col-md-6"><?php the_field('3_career'); ?></li>
            <li class="career-block__item col-lg-6 col-md-6"><?php the_field('4_career'); ?></li>
            <li class="career-block__item col-lg-6 col-md-6"><?php the_field('5_career'); ?></li>
            <li class="career-block__item col-lg-6 col-md-6"><?php the_field('6_career'); ?></li>
            <li class="career-block__item col-lg-6 col-md-6"><?php the_field('7_career'); ?></li>
            <li class="career-block__item col-lg-6 col-md-6"><?php the_field('8_career'); ?></li>
          </ul>

        </div>
      </div>

      <div class="row">
        <div class="vacancies">
          <h2 class="vacancies__title"><?php the_field('title_career_2'); ?></h2>
        </div>

        <?php
          // параметры по умолчанию
          $my_posts = get_posts( array(
            'numberposts' => -1,
            'category_name'    => 'actual_career',
            'orderby'     => 'date',
            'order'       => 'ASC',
            'post_type'   => 'post',
            'suppress_filters' => true, // подавление работы фильтров изменения SQL запроса
          ) );

          foreach( $my_posts as $post ){
            setup_postdata( $post );
            ?>

          <div class="row">
            <div class="card-vacancie row card-equipment__revers">
              <img src="<?php the_field('img_career'); ?>" alt="" class="card-vacancie__img col-lg-6 col-md-6">
              <div class="card-vacancie__descr col-lg-6 col-md-6">
                <h5 class="card-vacancie__title"><?php the_field('category_career'); ?></h5>
              <div class="card-vacancie__wrapper">
                <span class="card-vacancie__card-title"><?php the_field('name_career'); ?></span>
                <ul class="card-vacancie__list">
                  <li class="card-vacancie__item list-reset"><?php the_field('1_condition'); ?></li>
                  <li class="card-vacancie__item list-reset"><?php the_field('2_condition'); ?></li>
                  <li class="card-vacancie__item list-reset"><?php the_field('3_condition'); ?></li>
                  <li class="card-vacancie__item list-reset"><?php the_field('4_condition'); ?></li>
                </ul>
                <button class="card-vacancie__btn"><?php the_field('btn_career'); ?></button>
              </div>
              <div class="wrap-form">
                          <form action="POST" class="form-product">
              <?php echo do_shortcode('[contact-form-7 id="524" title="Отклик по вакансии"]'); ?>
            </form>
            </div>
           </div>
          </div>

            <?php
          }

          wp_reset_postdata(); // сброс
        ?>

      </div>
    </div>
  </section>
</body>

</html>

<?php
  get_footer();
?>

