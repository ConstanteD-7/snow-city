<?php
  /*
  Template Name: Карточка товара
  */
?>

<?php
  get_header();
?>

<body>
  <section class="page-product">
    <div class="container">
      <div class="row">
        <div class="col page-product__bread-crumbs">
          <div class="bread-crumbs">
            <ul class="bread-crumbs__list list-reset">
              <li class="bread-crumbs__item"><a href="#" class="bread-crumbs__link">Каталог продукции /</a></li>
              <li class="bread-crumbs__item"><a href="#" class="bread-crumbs__link">Пельмени из говядины</a></li>
            </ul>
          </div>
          <a href="#" class="section-btn-link">Вернуться в каталог</a>
        </div>

      </div>
      <div class="row">
        <h1 class="page-product__title">Пельмени из&nbsp;говядины &laquo;Пичугино&raquo;</h1>
      </div>
      <div class="row">
        <div class="col-lg-5 col-md-5 page-product__wrapper-img">
          <img src="<?php echo bloginfo('template_url'); ?>/assets/img/card-product.jpg" alt="фото продукта" class="page-product__img">
        </div>
        <div class="col-lg-7 col-md-7 page-product__wrapper">
          <div class="page-product__description">
            <h5 class="page-product__descr-title">Описание продукта и&nbsp;состав:</h5>
          </div>
          <ul class="page-product__list list-reset">
            <li class="page-product__item">
              <p class="page-product__text">Есть над чем задуматься: интерактивные прототипы неоднозначны и&nbsp;будут
                обнародованы. Для современного мира постоянный количественный рост.
                Есть над чем задуматься: интерактивные прототипы неоднозначны и&nbsp;будут обнародованы.</p>
            </li>
            <li class="page-product__item"> <span>Вес продукта: </span>900&nbsp;гр
            </li>
            <li class="page-product__item"><span>Условия хранения:
              </span>24&nbsp;месяца, до&nbsp;18&nbsp;&deg;C</li>
            <li class="page-product__item"><span>Пищевая ценность: </span>белки
              12&nbsp;гр, Жиры&nbsp;&mdash; 12&nbsp;гр, Углеводы
              24&nbsp;гр</li>
            <li class="page-product__item"><span>Энергетическая ценность:
              </span>1820кДж/300 ккал</li>
          </ul>
        </div>
      </div>
      <div class="row">
        <div class="page-product__wrapper-form">
          <div class="col-lg-5 col-md-5 page-product__form">
            <h5 class="page-product__form-title">Оставьте заявку на&nbsp;заказ продукции</h5>
            <form action="POST" class="form-product">
              <input class="form-product__name" type="text" placeholder="Имя">
              <input class="form-product__tel" type="tel" placeholder="Телефон">
              <textarea name="form-product__textarea" id="form-product__textarea" cols="30" rows="1" placeholder="Комменатрий"></textarea>
              <input class="form-product__btn btn" type="submit" value="Оформить заказ">
            </form>
          </div>
          <div class="col-lg-6 col-md-6 page-product__wrappe-video">
            <div class="product-video">
              <img src="<?php echo bloginfo('template_url'); ?>/assets/img/production.jpg" alt="">
              <span class="product-video__title">Посмотрите видео с&nbsp;нашего производства</span>
              <button class="product-video__play"></button>
            </div>
          </div>
        </div>

      </div>
    </div>
  </section>
</body>

</html>

<?php
  get_footer();
?>

