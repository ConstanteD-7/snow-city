<?php
  /*
  Template Name: Сертификаты
  */
?>

<?php
  get_header();
?>

<body>
  <section class="page-certificates">
    <div class="container">
      <div class="row">
        <h1 class="page-certificates__title col-lg-9 col-md-9"><?php the_field('certificates_title'); ?></h1>
      </div>
      <div class="row">
        <?php
          // параметры по умолчанию
          $my_posts = get_posts( array(
            'numberposts' => -1,
            'category_name'    => 'certificates',
            'orderby'     => 'date',
            'order'       => 'ASC',
            'post_type'   => 'post',
            'suppress_filters' => true, // подавление работы фильтров изменения SQL запроса
          ) );

          foreach( $my_posts as $post ){
            setup_postdata( $post );
            ?>

            <div class="card-certificat col-lg-4 col-md-4 col-6">
              <img src="<?php the_field('certificates_img'); ?>">
              <button class="card-certificat__btn"></button>
            </div>

            <?php
          }

          wp_reset_postdata(); // сброс
        ?>
      </div>
    </div>
  </section>

</body>

</html>

<?php
  get_footer();
?>
