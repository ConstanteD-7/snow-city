<?php
  get_header();
?>
  <main class="main">
    <section class="hero">
      <div class="container hero__container">
        <div class="row">

          <div class="hero-contain">
            <div class="hero__promo-bg">
              <div class="hero__content col-lg-5 col-md-6">
                <!-- Подключаем группу полей из админки, а именно заголовок -->
                <h1 class="hero__title">
                  <?php the_field('about_title'); ?>
                </h1>
                <!-- Подключаем группу полей из админки, а именно описание -->
                <p class="hero__description">
                  <?php the_field('about_descr'); ?>
                </p>
                <!-- Подключаем группу полей из админки, а текст внутри кнопки -->
                <a href="/katalog-produkczii" class="hero__btn">
                  <?php the_field('text_button'); ?></a>
              </div>
            <!-- Подключаем группу полей из админки, а именно презентационное изображение -->
              <img src="<?php the_field('about_img'); ?>" alt="про компанию">
            </div>
          </div>
        </div>
      </div>
    </section>
    <section class="popular-goods">
      <div class="container">
        <div class="row">
           <!-- Подключаем группу полей из админки, а именно заголовок секции -->
          <h2 class="popular-goods__title col">
            <?php the_field('title_catalog'); ?></h2>
          <a href="/katalog-produkczii" class="section-btn-link col-lg-3">Смотреть весь каталог</a>
        </div>
        <div class="row">
          <div class="popular-goods__wrapper-content">

            <?php
              // параметры по умолчанию
              $my_posts = get_posts( array(
                'numberposts' => 8,
                'category_name'    => 'catalog',
                'orderby'     => 'date',
                'order'       => 'ASC',
                'post_type'   => 'post',
                'suppress_filters' => true, // подавление работы фильтров изменения SQL запроса
              ) );

              foreach( $my_posts as $post ){
                setup_postdata( $post );
                ?>

                  <div class="card-product col">
                    <img src="<?php the_field('image_product'); ?>" alt="Изображеие товара" class="card-product__img">
                    <div class="card-product__content">
                      <span class="card-product__title"><?php the_title( ); ?></span>
                      <span class="card-product__description"><?php the_field('weight_product'); ?></span>
                      <a href="<?php echo get_permalink(); ?>" class="card-product__btn-link btn-link"><?php the_field('link_product'); ?></a>
                    </div>
                  </div>
                <?php
              }

              wp_reset_postdata(); // сброс
            ?>
          </div>
        </div>
      </div>
    </section>
    <section class="buy-where">
      <div class="container">
        <div class="row">
          <h2 class="buy-where__title"><?php the_field('title_map'); ?></h2>
        </div>
        <div class="row">
          <div class="buy-where maps" id="map" style="width: 100%; height: 432px">
            <div class="maps__maps-info maps-info">
              <span class="maps-info__title">Магазины сети:</span>
              <input class="maps-info__search" type="search" name="maps-info__search" id="maps-info__search"
                placeholder="Начните вводить адрес">
              <button class="maps-info__btn"></button>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section class="our-production">
      <div class="container">
        <div class="row">
          <h2 class="our-production__title"><?php the_field('title_production'); ?></h2>
        </div>
        <div class="row our-production__wrapper">

          <!-- Встроить видео -->
          <div class="product-video col-lg-6 col-md-6">
            <video width="100%" controls loop preload="none" poster="<?php bloginfo('template_url'); ?>/assets/img/poster-video.jpg">
              <source src="<?php the_field('video_production'); ?>" type="video/mp4">
            </video>
          </div>


          <div class="our-production__wrapper-text col-lg-6 col-md-6">
            <p class="our-production__text"><?php the_field('descr_production'); ?></p>
            <a href="/o-kompanii" class="our-production__btn-link btn-link">Подробнее о&nbsp;компании</a>
          </div>
        </div>
      </div>
    </section>
    <section class="articles-and-news">
      <div class="container">
        <div class="row">
          <h2 class="articles-and-news__title"><?php the_field('title_news'); ?></h2>
        </div>
        <div class="row">
          <div class="articles-and-news__wrapper">
          <?php
              // параметры по умолчанию
              $my_posts = get_posts( array(
                'numberposts' => 4,
                'category_name'    => 'news',
                'orderby'     => 'date',
                'order'       => 'ASC',
                'post_type'   => 'post',
                'suppress_filters' => true, // подавление работы фильтров изменения SQL запроса
              ) );

              foreach( $my_posts as $post ){
                setup_postdata( $post );
                ?>

                <div class="card-articles col-lg-4 col-md-4">
                  <img src="<?php the_field('img_news'); ?>" alt="Изображение новости" class="card-articles__img">
                  <div class="card-articles__content">
                    <div class="card-articles__wrap">
                      <div class="card-articles__info">
                        <span class="card-articles__tag"><?php the_field('type_news'); ?></span>
                        <span class="card-articles__date"><?php the_field('date_news'); ?></span>
                      </div>
                      <span class="card-articles__title"><?php the_field('title_news'); ?></span>
                      <div class="card-articles__body">
                        <p class="card-articles__text"><?php the_field('descr_news'); ?></p>
                      </div>
                      <a  class="card-articles__btn-link btn-link">Подробнее</a>
                    </div>
                  </div>
                </div>

                <?php
              }

                wp_reset_postdata(); // сброс
              ?>

              <?php
                // параметры по умолчанию
                $my_posts = get_posts( array(
                  'numberposts' => 1,
                  'category_name'    => 'posts',
                  'orderby'     => 'date',
                  'order'       => 'DESC',
                  'post_type'   => 'post',
                  'suppress_filters' => true, // подавление работы фильтров изменения SQL запроса
                ) );

                foreach( $my_posts as $post ){
                  setup_postdata( $post );
                  ?>

                  <div class="card-articles card-articles--big col-lg-8">
                    <img src="<?php the_field('img_post'); ?>" alt="Изображение новости"
                      class="card-articles__img">
                    <div class="card-articles__content">
                      <div class="card-articles__wrap">
                        <div class="card-articles__info">
                          <span class="card-articles__tag"><?php the_field('img_type'); ?></span>
                          <span class="card-articles__date"><?php the_field('date_post'); ?></span>
                        </div>
                        <span class="card-articles__title"><?php the_field('title_post'); ?></span>
                        <div class="card-articles__body">
                          <p class="card-articles__text"><?php the_field('text_post'); ?></p>
                        </div>
                        <a  class="card-articles__btn-link btn-link">Подробнее</a>
                      </div>
                    </div>
                  </div>
                  <?php
                }

                wp_reset_postdata(); // сброс
              ?>
          </div>
        </div>
      </div>
    </section>
  </main>

<?php
  get_footer();
?>
