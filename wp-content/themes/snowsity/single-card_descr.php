<?php
  /*
  Template Name: Шаблон подробной карточки товара
  Template Post Type: post
  */
?>


<?php
  get_header();
?>

  <?php
  /* Start the Loop */
    while ( have_posts() ) :
      the_post();

      get_template_part( 'template/content', get_post_type() );


    endwhile; // End of the loop.
  ?>

  <body>
  <section class="page-product">
    <div class="container">
      <div class="row">
        <div class="col page-product__bread-crumbs">

          <?php
            if ( function_exists('yoast_breadcrumb') ) {
              yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
            }
          ?>

          <a href="/katalog-produkczii" class="section-btn-link">Вернуться в каталог</a>
        </div>

      </div>
      <div class="row">
        <h1 class="page-product__title"><?php the_field('home_title_product'); ?></h1>
      </div>
      <div class="row mb-60">
        <div class="col-lg-5 col-md-5 page-product__wrapper-img">
          <img src="<?php the_field('home_img_product'); ?>" alt="фото продукта" class="page-product__img">
        </div>
        <div class="col-lg-7 col-md-7 page-product__wrapper">
          <div class="page-product__description">
            <h5 class="page-product__descr-title">Описание продукта и&nbsp;состав:</h5>
          </div>
          <ul class="page-product__list list-reset">
            <li class="page-product__item">
              <p class="page-product__text"><?php the_field('home_descr_product'); ?></p>
            </li>
            <li class="page-product__item"> <span>Вес продукта: </span><?php the_field('home_weight_product'); ?>
            </li>
            <li class="page-product__item"><span>Условия хранения:
              </span><?php the_field('home_condition_product'); ?></li>
            <li class="page-product__item"><span>Пищевая ценность: </span><?php the_field('home_value_product'); ?></li>
            <li class="page-product__item"><span>Энергетическая ценность:
              </span><?php the_field('home_energy_product'); ?></li>
          </ul>
        </div>
      </div>
      <div class="row">
        <div class="page-product__wrapper-form">
          <div class="col-lg-5 col-md-5 page-product__form">
            <h5 class="page-product__form-title">Оставьте заявку на&nbsp;заказ продукции</h5>
            <form action="POST" class="form-product">
              <?php echo do_shortcode('[contact-form-7 id="406" title="Заявка на заказ продукции"]'); ?>
              <!-- <input id="form-product__name" class="form-product__name" type="text" placeholder="Имя">
              <input id="form-product__tel" class="form-product__tel" type="tel" placeholder="Телефон">
              <textarea id="form-product__textarea" name="form-product__textarea" cols="30" rows="1"
                placeholder="Комменатрий"></textarea>
              <input class="form-product__btn btn" type="submit" value="Оформить заказ"> -->
            </form>
          </div>
          <div class="product-video col-lg-6 col-md-6">
          <video width="100%" controls="controls" loop preload="none" poster="<?php bloginfo('template_url'); ?>/assets/img/poster-video.jpg">
            <source src="<?php the_field('video_production', 2); ?>">
          </video>
        </div>
        </div>

      </div>
    </div>
  </section>
</body>

<?php
  get_footer();
?>
