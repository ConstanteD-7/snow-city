<!DOCTYPE html>
<html lang="ru">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="preconnect" href="https://fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
  <link href="https://fonts.googleapis.com/css2?family=Jost:wght@300;400;500&family=Roboto:wght@400;500&display=swap"
    rel="stylesheet">

  <!-- Заменяем статичный заголовок во вкладке браузера на динамичный php -->
  <title><?php bloginfo('name'); echo " | "; bloginfo('description'); ?></title>
  <script src="https://api-maps.yandex.ru/2.1/?apikey=ваш API-ключ&lang=ru_RU"></script>

  <?php
    wp_head();
  ?>

</head>

<body>

  <header class="header">

    <div class="header__top-header">
      <div class="container">

        <div class="top-header">
          <div class="top-header__logo logo">
            <div class="logo__link">
              <!-- Подключаем динамический логотип здесь и в футере -->
              <!-- ссылку замени на див -->
              <?php the_custom_logo(); ?>
            </div>
          </div>
          <div class="adress">
            <!-- Добавляем ввод адреса в админке -->
            <address class="adress__text"><?php the_field('address_info', 2); ?></address>
          </div>
          <div class="clock-work">
            <span class="clock-work__text"><?php the_field('time_job', 2); ?></span>
          </div>

          <div class="phone">
            <a href="tel:+79225600044" class="phone__link"><?php the_field('tel_2', 2); ?></a>
            <a href="tel:+79226762222" class="phone__link"><?php the_field('tel_3', 2); ?></a>
          </div>

          <div class="mail-tel">
             <a href="tel:+73523328465" class="phone-number__link"><?php the_field('tel_1', 2); ?></a>
             <a href="email:info@icegrad.com" class="email__link"><?php the_field('mail', 2); ?></a>
          </div>

          <button class="burger"></button>
        </div>
      </div>
    </div>

    <div class="header__bottom-header">
      <div class="bottom-header container">
        <nav class="header-nav">
          <?php wp_nav_menu([
            'container ' => false,
          ]);?>
          <div class="adress">
            <!-- Добавляем ввод адреса в админке -->
            <address class="adress__text"><?php the_field('address_info', 2); ?></address>
          </div>
          <div class="clock-work">
            <span class="clock-work__text"><?php the_field('time_job', 2); ?></span>
          </div>
          <div class="mail-tel">
             <a href="tel+73523328465" class="phone-number__link"><?php the_field('tel_1', 2); ?></a>
             <a href="email:info@icegrad.com" class="email__link"><?php the_field('mail', 2); ?></a>
          </div>
        </nav>
      </div>

    </div>
  </header>
