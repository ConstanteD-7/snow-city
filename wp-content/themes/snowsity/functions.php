<?php

  // Добавляем действия
  add_action('wp_enqueue_scripts', 'snowcity_scripts');

  // Подключаем файл со стилями
  function snowcity_scripts() {
    wp_enqueue_style( 'snowcity-styles', get_stylesheet_uri() );
    wp_enqueue_style( 'snowcity-normalize', get_stylesheet_directory_uri() . '/assets/css/normalize.css');
    wp_enqueue_style( 'snowcity-bootstrap', get_stylesheet_directory_uri() . '/assets/css/bootstrap-grid.min.css');
    wp_enqueue_script( 'snowcity-scripts', get_template_directory_uri() . '/assets/js/index.js', array('jquery'), null, true);




  };



  // Добавляет в админке функцию выбрать логотип. Не забудь в хедере прописать подключение этого лого.
  add_theme_support( 'custom-logo' );
  add_theme_support('widgets');

  // // Подключаем файл со скриптами
  // function childhood_scripts() {
  //   wp_enqueue_script( 'childhood-scripts', get_template_directory_uri() . '/assets/js/main.min.js', array('jquery'), null, true);
  //   // wp_enqueue_style( 'headerd-style', get_template_directory_uri() . '/assets/styles/main.min.css');
  //   // wp_enqueue_style( 'animate-style', get_template_directory_uri() . 'https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css');
  // };

  // // Добавляет в админке функцию выбрать логотип. Не забудь в хедере прописать подключение этого лого.
  // add_theme_support( 'custom-logo' );

  // // Добавляет в админке функцию превью "изображение записей" для предварительного отображения постов мягких игрушек.
  // add_theme_support( 'post-thumbnails' );


  //menu

  add_action( 'after_setup_theme', 'theme_register_nav_menu' );

function theme_register_nav_menu() {
	register_nav_menu( 'primary', 'Primary Menu' );
}

//classes all menu


add_filter( 'nav_menu_css_class', 'add_my_class_to_nav_menu', 10, 2 );
function add_my_class_to_nav_menu( $classes, $item ){



	$classes[] = 'my__class';

	return $classes;
}

?>
