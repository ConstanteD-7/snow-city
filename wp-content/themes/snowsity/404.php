<?php
  /*
  Template Name: Страница 404
  */
?>


<?php
  get_header();
?>


<body>
  <section class="page-404">
    <div class="container">
      <div class="row justify-content-center">
        <h1 class="page-404__title">ОШИБКА 404</h1>
        <div class="page-404__wrapper col-lg-8 col-md-8">
          <p class="page-404__text ">Кажется что-то пошло не&nbsp;так! Страница, которую вы&nbsp;запрашиваете,
            не&nbsp;существует. Возможно она
            устарела, была удалена, или был введен неверный адрес в&nbsp;адресной строке</p>
          <a href="http://snow-city" class="page-404__link-btn">Вернуться на&nbsp;главную</a>
        </div>

      </div>
    </div>
  </section>
</body>

</html>

<?php
  get_footer();
?>

