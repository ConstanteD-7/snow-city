function init() {

  var mapProduct,
    mapOffice,
    myMap;

  if (document.querySelector('#map')) {
    myMap = new ymaps.Map("map", {
      center: [55.448867, 65.372927],
      zoom: 9,
      controls: []
    });
  } else if (document.querySelector('#map-contact-product') && document.querySelector('#map-contact-office')) {
    mapProduct = new ymaps.Map("map-contact-product", {
      center: [55.444241, 65.803149],
      zoom: 15,
      controls: []
    });
    mapOffice = new ymaps.Map('map-contact-office', {
      center: [55.448867, 65.372927],
      zoom: 15,
      controls: []
    });
  };
  // Создаем коллекцию.
  var myCollection = new ymaps.GeoObjectCollection(),
    // Создаем массив с данными.
    myPoints = [{
        coords: [55.448867, 65.372927],
        text: 'Трактир',
        name: "ул. М. Горького, д. 238, оф. 29",
        hours: 'Пн- вс: 08:00 до 23:00'
      },
      {
        coords: [55.440110, 65.340256],
        text: 'Кафе',
        name: "ул. ​Гоголя, д.55, ​1 этаж",
        hours: 'Пн- вс: 08:00 до 23:00'
      },
      {
        coords: [55.4241, 65.803149],
        text: 'Ресторан',
        name: "ул. Ленина, д. 23, ",
        hours: 'Пн- вс: 08:00 до 23:00'
      },
      {
        coords: [55.45, 65.38],
        text: 'Музей',
        name: "ул. К. Маркса, д. 140, оф. 9",
        hours: 'Пн- вс: 08:00 до 23:00'
      },
      {
        coords: [55.43, 65.33],
        text: 'Библиотека',
        name: "ул. М. Горького, д. 28, ",
        hours: 'Пн- вс: 08:00 до 23:00'
      }
    ];

  // Заполняем коллекцию данными.
  for (var i = 0, l = myPoints.length; i < l; i++) {
    var point = myPoints[i];
    myCollection.add(new ymaps.Placemark(
      point.coords, {
        balloonContentBody: `<div>${point.name}<br>${point.hours}</div>`
      }, {
        iconLayout: 'default#image',
        iconImageHref: '../../wp-content/themes/snowsity/assets/img/svg/icon-geo-cart.svg',
        iconImageSize: [24, 29],
        iconImageOffset: [-12, -29]
      }
    ));
  }
  if (myMap !== undefined) {
    myMap.geoObjects.add(myCollection);
  }

  // Добавляем коллекцию меток на карту.
  if (mapProduct !== undefined || mapOffice !== undefined) {
    mapProduct.geoObjects.add((new ymaps.Placemark(
      [55.444241, 65.803149], {
        balloonContentBody: `<div>с. Пичугино, ул. Социалистическая, 6<br>Пн- вс: 08:00 до 23:00</div>`
      }, {
        iconLayout: 'default#image',
        iconImageHref: '../../wp-content/themes/snowsity/assets/img/svg/icon-geo-cart.svg',
        iconImageSize: [24, 29],
        iconImageOffset: [-12, -29]
      }
    )));
    mapOffice.geoObjects.add((new ymaps.Placemark(
      [55.448867, 65.372927], {
        balloonContentBody: `<div>ул. М. Горького, д. 28, <br>Пн- вс: 08:00 до 23:00</div>`
      }, {
        iconLayout: 'default#image',
        iconImageHref: '../../wp-content/themes/snowsity/assets/img/svg/icon-geo-cart.svg',
        iconImageSize: [24, 29],
        iconImageOffset: [-12, -29]
      }
    )));
  }


  // Создаем экземпляр класса ymaps.control.SearchControl
  var mySearchControl = new ymaps.control.SearchControl({
    options: {
      // Заменяем стандартный провайдер данных (геокодер) нашим собственным.
      provider: new CustomSearchProvider(myPoints),
      // Не будем показывать еще одну метку при выборе результата поиска,
      // т.к. метки коллекции myCollection уже добавлены на карту.
      noPlacemark: true,
      resultsPerPage: 5
    }
  });

  // Добавляем контрол в верхний правый угол,
  if (myMap !== undefined) {
    myMap.controls
      .add(mySearchControl, {
        float: 'right'
      });
  }

}


// Провайдер данных для элемента управления ymaps.control.SearchControl.
// Осуществляет поиск геообъектов в по массиву points.
// Реализует интерфейс IGeocodeProvider.
function CustomSearchProvider(points) {
  this.points = points;
}

// Провайдер ищет по полю text стандартным методом String.ptototype.indexOf.
CustomSearchProvider.prototype.geocode = function (request, options) {
  var deferred = new ymaps.vow.defer(),
    geoObjects = new ymaps.GeoObjectCollection(),
    // Сколько результатов нужно пропустить.
    offset = options.skip || 0,
    // Количество возвращаемых результатов.
    limit = options.results || 20;

  var points = [];
  // Ищем в свойстве text каждого элемента массива. // TEXT замении на NAME
  for (var i = 0, l = this.points.length; i < l; i++) {
    var point = this.points[i];
    if (point.name.toLowerCase().indexOf(request.toLowerCase()) != -1) {
      points.push(point);
    }
  }
  points = points.splice(offset, limit);
  for (var i = 0, l = points.length; i < l; i++) {
    var point = points[i],
      coords = point.coords,
      name = point.name;
    geoObjects.add(new ymaps.Placemark(coords, {
      name: name,
      description: point.hours,
      balloonContentBody: '<p>' + name + '</p>',
      boundedBy: [coords, coords]
    }));
  }

  deferred.resolve({
    // Геообъекты поисковой выдачи.
    geoObjects: geoObjects,
    // Метаинформация ответа.
    metaData: {
      geocoder: {
        // Строка обработанного запроса.
        request: request,
        // Количество найденных результатов.
        found: geoObjects.getLength(),
        // Количество возвращенных результатов.
        results: limit,
        // Количество пропущенных результатов.
        skip: offset
      }
    }
  });

  // Возвращаем объект-обещание.
  return deferred.promise();
};


let burger = (classBurger, classHeader, classBurgerStatus) => {
  let btnburger = document.querySelector(`.${classBurger}`),
    btnStatus = document.querySelector(`.${classHeader}`);

  if (btnburger) {
    btnburger.addEventListener('click', (e) => {
      let target = e.target;
      if (target && target.classList.contains(classBurger)) {
        btnStatus.classList.toggle(classBurgerStatus);
      }
    });
  }
};

function popup(clsasTrigger, wrapp, popupBody) {
  if (document.querySelectorAll(`.${wrapp}`).length > 0) {

    if (!document.querySelector('.popup')) {
      document.body.insertAdjacentHTML('beforeend', `
    <div class="popup">
      <div class="popup__content">
        <button class="popup__close"></button>
        <img class="card-articles__img">
      </div>
    </div>
    `);
    };

    const popup = document.querySelector('.popup'),
      popupContent = popup.querySelector('.popup__content'),
      popupImg = popup.querySelector('.card-articles__img');

    if (popup && wrapp) {
      let wrapper = document.querySelectorAll(`.${wrapp}`),
        content;
      wrapper.forEach(elem => {
        elem.addEventListener('click', (e) => {
          let target = e.target;
          if (target.classList.contains(clsasTrigger)) {
            const scrollWidth = window.innerWidth - document.documentElement.clientWidth;
            document.querySelector('body').style.marginRight = `${scrollWidth}px`;
            document.querySelector('body').style.overflow = 'hidden';

            if (elem.querySelector('img')) {
              popupImg.src = elem.querySelector('img').src;
            }
            if (elem.querySelector(`.${popupBody}`)) {

              content = elem.querySelector(`.${popupBody}`);
              content = content.cloneNode(content);
              popupContent.appendChild(content);
            }
            popup.classList.add('activ');
          }
        });
      });

      popup.addEventListener('click', (e) => {
        let target = e.target;
        if (target && (target.classList.contains('popup__close') || target.classList.contains('popup'))) {
          popup.classList.remove('activ');
          if (popupContent.querySelector(`.${popupBody}`)) {
            popupContent.removeChild(content);
          }
          setTimeout(() => {
            document.querySelector('body').style.marginRight = 0;
            document.querySelector('body').style.overflow = '';
          }, 200);
        }
      });
    }
  }
};


let accardeon = (classAccardon, classAccardonContent, classAccardonTitle, classAccardonBtn, classActive) => {
  const classAccardonAll = document.querySelectorAll(`.${classAccardon}`);

  if (classAccardonAll) {
    classAccardonAll.forEach(elem => {
      elem.addEventListener('click', (e) => {
        let target = e.target;
        let text = elem.querySelector(`.${classAccardonContent}`);
        if (target && (target.classList.contains(classAccardon) !==
            target.classList.contains(classAccardonTitle) !==
            target.classList.contains(classAccardonBtn))) {

          classAccardonAll.forEach(element => {
            if (element !== elem) {
              element.classList.remove(classActive);
              let textDel = element.querySelector(`.${classAccardonContent}`);
              textDel.classList.remove(classActive);
              textDel.style.maxHeight = '0px';
            }
          });
          elem.classList.toggle(classActive);
          text.classList.toggle(classActive);
          if (text.classList.contains(classActive)) {
            text.style.maxHeight = text.scrollHeight + 'px';
          } else {
            text.style.maxHeight = '0px';
          }

        }
      });
    });
  }
}

window.addEventListener('DOMContentLoaded', () => {
  burger('burger', 'header', 'burger-open');
  popup('card-articles__btn-link', 'card-articles', 'card-articles__wrap');
  accardeon('accordion-questions', 'accordion-questions__text', 'accordion-questions__title', 'accordion-questions__btn', 'accordion-open');
  popup('card-certificat__btn', 'card-certificat');
  popup('gallery__img', 'gallery__item');
  popup('stap-partner__btn--popup', 'stap-partner', 'wrap-form');
  popup('card-vacancie__btn', 'card-vacancie', 'wrap-form');

  ymaps.ready(init);
});
