<?php
/**
 * Основные параметры WordPress.
 *
 * Скрипт для создания wp-config.php использует этот файл в процессе установки.
 * Необязательно использовать веб-интерфейс, можно скопировать файл в "wp-config.php"
 * и заполнить значения вручную.
 *
 * Этот файл содержит следующие параметры:
 *
 * * Настройки MySQL
 * * Секретные ключи
 * * Префикс таблиц базы данных
 * * ABSPATH
 *
 * @link https://ru.wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Параметры базы данных: Эту информацию можно получить у вашего хостинг-провайдера ** //
/** Имя базы данных для WordPress */
define( 'DB_NAME', 'snow_city' );

/** Имя пользователя базы данных */
define( 'DB_USER', 'admin' );

/** Пароль к базе данных */
define( 'DB_PASSWORD', 'admin' );

/** Имя сервера базы данных */
define( 'DB_HOST', 'localhost' );

/** Кодировка базы данных для создания таблиц. */
define( 'DB_CHARSET', 'utf8mb4' );

/** Схема сопоставления. Не меняйте, если не уверены. */
define( 'DB_COLLATE', '' );

/**#@+
 * Уникальные ключи и соли для аутентификации.
 *
 * Смените значение каждой константы на уникальную фразу. Можно сгенерировать их с помощью
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ сервиса ключей на WordPress.org}.
 *
 * Можно изменить их, чтобы сделать существующие файлы cookies недействительными.
 * Пользователям потребуется авторизоваться снова.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'NCLLMYeYL/&O.4z&AYIVmia8r<CR=KAPvxw6=b:Q~-8FMQe5}aWu=xSZ62L7U.{Z' );
define( 'SECURE_AUTH_KEY',  '5LX|8fQ*~*pW`3qjU_*aJ,9)d+=FQ#A,BiciQPkG,blgUQP0Ycu|kGjEd8|iIu8e' );
define( 'LOGGED_IN_KEY',    'SW7QA,NTjmYf0@u${uWy{X63y`J;+Nn:<m 1i,`~pczp.j(D&[a)bFwL8l}rqTSE' );
define( 'NONCE_KEY',        '`i)KIEkS:JACuvyVwAwaR+V]_s.H,)KqIyzzPw9!$Vyh?eL 7]TcPUtkc[r7SLB]' );
define( 'AUTH_SALT',        'oJYP@#wk~7z9r$!0p7;@b_u2p3oS]:4xj0OWtN82|R*8@`i@v>Oj(8o$Gk.H/SWg' );
define( 'SECURE_AUTH_SALT', 'g/sW~:K,,]jI$<zh98JAaH0&eyBYA{.sXo$n-`^OZG|`oS%(6/{pY}ra]D~(v#9_' );
define( 'LOGGED_IN_SALT',   'pI Y~?nem691Ak06Zs_ULwNiK)@)dJE{|48b(jkj6s7zE*;E5Ue)YSj3W8yD/D2V' );
define( 'NONCE_SALT',       'F7Yr#W]kwMxb=T?Eq/B?P _Fk/6Ke@-*L@gOiD.a`GMM?[0{>@OoM%:4;0%7V9(Z' );

/**#@-*/

/**
 * Префикс таблиц в базе данных WordPress.
 *
 * Можно установить несколько сайтов в одну базу данных, если использовать
 * разные префиксы. Пожалуйста, указывайте только цифры, буквы и знак подчеркивания.
 */
$table_prefix = 'wp_';

/**
 * Для разработчиков: Режим отладки WordPress.
 *
 * Измените это значение на true, чтобы включить отображение уведомлений при разработке.
 * Разработчикам плагинов и тем настоятельно рекомендуется использовать WP_DEBUG
 * в своём рабочем окружении.
 *
 * Информацию о других отладочных константах можно найти в документации.
 *
 * @link https://ru.wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Произвольные значения добавляйте между этой строкой и надписью "дальше не редактируем". */



/* Это всё, дальше не редактируем. Успехов! */

/** Абсолютный путь к директории WordPress. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Инициализирует переменные WordPress и подключает файлы. */
require_once ABSPATH . 'wp-settings.php';
